package com.aem.oxygen.core.workflows;

import java.io.InputStream;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.aem.oxygen.core.services.api.ResourceResolverService;


@Component(service = WorkflowProcess.class, immediate = true, property = {
		Constants.SERVICE_DESCRIPTION + "=Avalara Oxygen Integration : Convert File to Asset",
		"process.label=Avalara Oxygen Integration Convert File to Asset"
		 })
public class ConvertFileToAssetWorkflow implements WorkflowProcess {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConvertFileToAssetWorkflow.class);

	@Reference
	protected ResourceResolverService resourceResolverService;

	@Override
	public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap metaDataMap)
			throws WorkflowException {
		InputStream is = null;
		Node node = null;
		LOGGER.info(" <== Inside ConvertFileToAssetWorkflow Workflow =>");

		String payload = (String) workItem.getWorkflowData().getPayload();

		LOGGER.info("Current Payload ::{}", payload);

		ResourceResolver resourceResolver = resourceResolverService.getWriteSystemResourceResolver();
		Resource fileResource = resourceResolver.getResource(payload);

		if (null != fileResource) {
			LOGGER.info("fileResource:{}", fileResource.getPath());
			node = fileResource.adaptTo(Node.class);
		}

		try {
			if (null != node) {
				is = node.getNode("jcr:content").getProperty("jcr:data").getBinary().getStream();
				LOGGER.info("Input Stream {}", is);

				String fileResourceName = fileResource.getName();

				String finalPath = "/content/dam/oxygen_aem/xml/".concat(fileResourceName).concat(".xml");

				LOGGER.info("Writing to DAM");
				com.day.cq.dam.api.AssetManager assetMgr = resourceResolver
						.adaptTo(com.day.cq.dam.api.AssetManager.class);
				assetMgr.createAsset(finalPath, is, "application/xml", true);
				LOGGER.info("Writing to DAM Completed ");
			}

		} catch (RepositoryException e) {
			LOGGER.info("Error while converting file to asset in DAM{}", e);
		}
	}
}
