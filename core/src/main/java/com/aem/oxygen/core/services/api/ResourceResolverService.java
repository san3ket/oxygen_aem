package com.aem.oxygen.core.services.api;

import org.apache.sling.api.resource.ResourceResolver;

/**
 * This interface has functions declared to get resource resolver of a particular service.
 */
public interface ResourceResolverService {


    ResourceResolver getWriteSystemResourceResolver();

}