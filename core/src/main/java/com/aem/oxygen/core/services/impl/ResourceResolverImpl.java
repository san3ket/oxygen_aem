package com.aem.oxygen.core.services.impl;



import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.oxygen.core.services.api.ResourceResolverService;


import java.util.HashMap;
import java.util.Map;

@Component(service = ResourceResolverService.class, immediate = true, property = {
		Constants.SERVICE_DESCRIPTION + "=Avalara Oxygen Integration with AEM Resource Resolver" })
public class ResourceResolverImpl implements ResourceResolverService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Reference
    private ResourceResolverFactory resourceFactory;



    /**
     * This method will fetch resource resolver of writeServiceUser.
     * User associated with this resolver is 'writeServiceUser'
     * @return ResourceResolver
     */
    public ResourceResolver getWriteSystemResourceResolver() {
        ResourceResolver resourceResolver = null;
        try {
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put(ResourceResolverFactory.SUBSERVICE, "writeServiceUserForOxygen");
            resourceResolver = resourceFactory.getServiceResourceResolver(paramMap);
        } catch (LoginException e) {
            log.error("Login Exception : " + e);
        }
        return resourceResolver;
    }
}